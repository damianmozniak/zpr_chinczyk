var Power = function(name, positionX, positionY, type) {

    this.name = name;
    this.type =  type !== undefined ? type : null;
    this.position = {
        x: positionX,
        y: positionY
    };
};

module.exports = Power;