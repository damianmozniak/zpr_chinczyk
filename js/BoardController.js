LUDO.BoardController = function (options) {
    'use strict';

    options = options || {};

    /**********************************************************************************************/
    /* Private properties *************************************************************************/

    /**
     * The DOM Element in which the drawing will happen.
     * @type HTMLDivElement
     */
    var containerEl = options.containerEl || null;

    /** @type String */
    var assetsUrl = options.assetsUrl || '';

    /** @type THREE.WebGLRenderer */
    var renderer;

    /** @type THREE.Scene */
    var scene;

    /** @type THREE.PerspectiveCamera */
    var camera;

    /** @type THREE.OrbitControls */
    var cameraController;

    /** @type Object */
    var lights = {};

    /** @type Object */
    var materials = {};

    /** @type THREE.Geometry */
    var pieceGeometry = null;

    /** @type THREE.Mesh */
    var boardModel;

    /** @type THREE.Mesh */
    var groundModel;

    var textureLoader;
    //TEXTURES
    var boardTexture;
    var lightSquareTexture;
    var darkSquareTexture;
    var groundTexture;
    var pieceShadowTexture;
    var particlesRing;
    var frostBolt;
    var INTERSECTED = null;
    var particleMaterial;
    var self = this;
    var savedMat = null;
    /**
     * The board square size.
     * @type Number
     * @constant
     */
    var squareSize = 7.28;

    var pickedPawn = null;

    var pickedPawn_ = null;

    this.yourTurn = options.yourTurn || false;

    var rolledNumber = 0;

    var foundPawn;

    socket.on('turnInfo', function (data) {
        self.yourTurn = self.playerColor === data.playerTurn;
        rolledNumber = data.rolledNumber;
        console.log('Turn Info updated');
        if (self.yourTurn) showAnnotation("Twój ruch !", 500);
    });

    /**********************************************************************************************/
    /* Public vars *****************************************************************************/
    this.inGame = false;
    this.playerColor = null;
    /**********************************************************************************************/
    /* Public methods *****************************************************************************/

    /**
     * Draws the board.
     */
    this.drawBoard = function (callback) {
        initEngine();
        initMenu();
        initLights();
        initTextures();
        initMaterials();

        initObjects(function () {
            onAnimationFrame();

            callback();
        });
        //devTest();
    };

    this.rollTheDice = function (num) {
        if (!num) {
            showAnnotation("Musisz wylosować 6, aby wyjść \nMoże w następnej kolejce Ci się poszczęści", 1500);
        } else if (typeof num === 'number') {
            rolledNumber = num;
            showAnnotation("Wylosowałeś " + num + "\nWybierz pionek, którym chcesz się ruszyć.");
        } else {
            showAnnotation("To nie jest Twoja tura\nPoczekaj na swoją kolej");
        }
    };

    this.setPawnsOnBoard = function () {
        socket.emit('getPawns', {}, function (pawns) {

            pawns.forEach(function (pawn) {
                var pieceMesh = new THREE.Mesh(pieceGeometry);
                var pieceObjGroup = new THREE.Object3D();

                if (pawn.color === LUDO.BLUE) {
                    pieceObjGroup.color = LUDO.BLUE;
                    pieceMesh.material = materials.bluePieceMaterial;
                } else if (pawn.color === LUDO.RED) {
                    pieceObjGroup.color = LUDO.RED;
                    pieceMesh.material = materials.redPieceMaterial;
                } else if (pawn.color === LUDO.YELLOW) {
                    pieceObjGroup.color = LUDO.YELLOW;
                    pieceMesh.material = materials.yellowPieceMaterial;
                } else {
                    pieceObjGroup.color = LUDO.GREEN;
                    pieceMesh.material = materials.greenPieceMaterial;
                }

                var shadowPlane = new THREE.Mesh(new THREE.PlaneGeometry(squareSize, squareSize, 1, 1), materials.pieceShadowPlane);
                shadowPlane.rotation.x = -90 * Math.PI / 180;
                shadowPlane.scale.x = 0.6;
                shadowPlane.scale.y = 0.6;

                pieceObjGroup.add(pieceMesh);
                pieceObjGroup.add(shadowPlane);
                pieceObjGroup.name = "Pawn";
                pieceObjGroup.boardId = pawn.id;
                pieceObjGroup.boardPosX = pawn.boardPos[0];
                pieceObjGroup.boardPosY = pawn.boardPos[1];
                pieceObjGroup.position.setX(boardToWorld(pawn.boardPos).x);
                pieceObjGroup.position.setZ(boardToWorld(pawn.boardPos).z);
                scene.add(pieceObjGroup);
            });
        });
    };

    /**********************************************************************************************/
    /* Private methods ****************************************************************************/

    /**
     * Initialize some basic 3D engine elements.
     */
    function initEngine() {
        var viewWidth = containerEl.offsetWidth;
        var viewHeight = containerEl.offsetHeight;

        renderer = new THREE.WebGLRenderer({
            antialias: true
        });

        renderer.setSize(viewWidth, viewHeight);
        renderer.setClearColor(0xfefefe, 1);
        scene = new THREE.Scene();
        camera = new THREE.PerspectiveCamera(35, viewWidth / viewHeight, 1, 1000);
        camera.position.set(squareSize * 4, 120, 150);
        cameraController = new THREE.OrbitControls(camera, containerEl);
        cameraController.center = new THREE.Vector3(squareSize * 5.5, 0, squareSize * 5.5);
        scene.add(camera);
        containerEl.appendChild(renderer.domElement);

    }

    /**
     * Initialize 3d menu.
     */
    function initMenu() {

    }

    /**
     * Initialize the lights.
     */
    function initLights() {
        // top light
        lights.topLight = new THREE.PointLight();
        lights.topLight.position.set(squareSize * 4, 150, squareSize * 4);
        lights.topLight.intensity = 0.4;

        // white's side light
        lights.whiteSideLight = new THREE.SpotLight();
        lights.whiteSideLight.position.set(squareSize * 4, 100, squareSize * 4 + 200);
        lights.whiteSideLight.intensity = 0.8;
        lights.whiteSideLight.shadow.camera.fov = 55;

        // black's side light
        lights.blackSideLight = new THREE.SpotLight();
        lights.blackSideLight.position.set(squareSize * 4, 100, squareSize * 4 - 200);
        lights.blackSideLight.intensity = 0.8;
        lights.blackSideLight.shadow.camera.fov = 55;

        // light that will follow the camera position
        lights.movingLight = new THREE.PointLight(0xf9edc9);
        lights.movingLight.position.set(0, 10, 0);
        lights.movingLight.intensity = 0.5;
        lights.movingLight.distance = 500;

        // add the lights in the scene
        scene.add(lights.topLight);
        scene.add(lights.whiteSideLight);
        scene.add(lights.blackSideLight);
        scene.add(lights.movingLight);
    }

    /**
     * Initialize textures.
     */
    function initTextures() {
        textureLoader = new THREE.TextureLoader();
        boardTexture = textureLoader.load(assetsUrl + 'board_ludo_texture.jpg');
        lightSquareTexture = textureLoader.load(assetsUrl + 'square_light_texture.jpg');
        darkSquareTexture = textureLoader.load(assetsUrl + 'square_dark_texture.jpg');
        groundTexture = textureLoader.load(assetsUrl + 'ground.png');
        pieceShadowTexture = textureLoader.load(assetsUrl + 'piece_shadow.png')
    }

    /**
     * Initialize the materials.
     */
    function initMaterials() {
        // board material
        materials.boardMaterial = new THREE.MeshLambertMaterial({
            map: boardTexture
        });

        // ground material
        materials.groundMaterial = new THREE.MeshBasicMaterial({
            transparent: true,
            map: groundTexture
        });

        // dark square material
        materials.darkSquareMaterial = new THREE.MeshLambertMaterial({
            map: darkSquareTexture
        });
        //
        // light square material
        materials.lightSquareMaterial = new THREE.MeshLambertMaterial({
            map: lightSquareTexture
        });

        // red piece material
        materials.redPieceMaterial = new THREE.MeshPhongMaterial({
            color: 0xff0000,
            shininess: 20,
            opacity: 0.5
        });

        // blue piece material
        materials.bluePieceMaterial = new THREE.MeshPhongMaterial({
            color: 0x3a47e4,
            shininess: 20,
            opacity: 0.5
        });


        // yellow piece material
        materials.yellowPieceMaterial = new THREE.MeshPhongMaterial({
            color: 0xffff00,
            shininess: 20,
            opacity: 0.5
        });

        // green piece material
        materials.greenPieceMaterial = new THREE.MeshPhongMaterial({
            color: 0x00ff40,
            shininess: 20,
            opacity: 0.5
        });

        // pieces shadow plane material
        materials.pieceShadowPlane = new THREE.MeshBasicMaterial({
            transparent: true,
            map: pieceShadowTexture
        });
    }

    /**
     * Initialize the objects.
     * @param {Object} callback Function to call when the objects have been loaded.
     */
    function initObjects(callback) {
        var loader = new THREE.JSONLoader();
        var totalObjectsToLoad = 2; // board + the piece
        var loadedObjects = 0; // count the loaded pieces

        // checks if all the objects have been loaded
        function checkLoad() {
            loadedObjects++;

            if (loadedObjects === totalObjectsToLoad && callback) {
                callback();
            }
        }

        // load board
        loader.load(assetsUrl + 'board.js', function (geom) {
            boardModel = new THREE.Mesh(geom, materials.boardMaterial);
            boardModel.position.y = -0.02;

            scene.add(boardModel);

            checkLoad();
        });

        // load piece
        loader.load(assetsUrl + 'Pawn.js', function (geometry) {
            pieceGeometry = geometry;
            checkLoad();
        });

        // add ground
        groundModel = new THREE.Mesh(new THREE.PlaneGeometry(100, 100, 1, 1), materials.groundMaterial);
        groundModel.position.set(squareSize * 5.5, -1.52, squareSize * 5.5);
        groundModel.rotation.x = -90 * Math.PI / 180;

        scene.add(groundModel);


        //particlesRing
        particlesRing = new THREE.Object3D();
        particlesRing.position.set(boardToWorld([5, 5]).x, 1, boardToWorld([5, 5]).z);
        particlesRing.visible = false;

        scene.add(particlesRing);

        particleMaterial = new THREE.SpriteMaterial({
            map: new THREE.CanvasTexture(generateSprite()),
            blending: THREE.AdditiveBlending
        });

        function initParticleRotation(particlesRing) {
            var tween = new TWEEN.Tween(particlesRing.rotation)
                .to({y: 2 * Math.PI}, 3000)
                .onComplete(function () {
                    particlesRing.rotation.y = 0;
                })
                .start();
            tween.chain(tween);
        }

        function generateSprite() {
            var canvas = document.createElement('canvas');
            canvas.width = 16;
            canvas.height = 16;
            var context = canvas.getContext('2d');
            var gradient = context.createRadialGradient(canvas.width / 2, canvas.height / 2, 0, canvas.width / 2, canvas.height / 2, canvas.width / 2);

            gradient.addColorStop(0, 'rgba(255,255,255,1)');
            gradient.addColorStop(0.2, 'rgba(0,255,255,1)');
            gradient.addColorStop(0.4, 'rgba(0,0,64,1)');
            gradient.addColorStop(1, 'rgba(0,0,0,1)');
            context.fillStyle = gradient;
            context.fillRect(0, 0, canvas.width, canvas.height);
            return canvas;
        }

        var ileCzasteczek = 25;

        for (var i = 1; i <= ileCzasteczek; i++) {
            var particle = new THREE.Object3D();
            particle.rotation.y = i * (2 * Math.PI / ileCzasteczek);
            particle.translateZ(2);
            particlesRing.add(particle);

            var sprite = new THREE.Sprite(particleMaterial);
            sprite.translateZ(1);
            sprite.scale.x = sprite.scale.y = Math.random() + 1;
            particle.add(sprite);
        }
        initParticleRotation(particlesRing);
    }

    function frostBoltShoot(startPosX, startPosY, desirePosX, desirePosY) {
        var a = new THREE.Vector3(boardToWorld([startPosX, startPosY]).x, 1, boardToWorld([startPosX, startPosY]).z);
        var b = new THREE.Vector3(boardToWorld([desirePosX, desirePosY]).x, 1, boardToWorld([desirePosX, desirePosY]).z);
        var distance = a.distanceTo( b );
        var moveDelay = distance * 20;

        var frostBolt = new THREE.Object3D();

        frostBolt.position.set(boardToWorld([startPosX, startPosY]).x, 1, boardToWorld([startPosX, startPosY]).z);

        frostBolt.lookAt( new THREE.Vector3( boardToWorld([desirePosX, desirePosY]).x, 1, boardToWorld([desirePosX, desirePosY]).z ));

        if(desirePosX >= startPosX)
            frostBolt.rotation.y += Math.PI / 2;
        else
            frostBolt.rotation.y -= Math.PI / 2;

        new TWEEN.Tween( frostBolt.position )
            .to( { x: boardToWorld([desirePosX, desirePosY]).x, y: 1, z: boardToWorld([desirePosX, desirePosY]).z }, moveDelay )
            .start();

        scene.add(frostBolt);

        setTimeout(function(){
            scene.remove(frostBolt);
        }, moveDelay + 500);

        function initParticle( particle, delay ) {
            var particle = this instanceof THREE.Sprite ? this : particle;
            var delay = delay !== undefined ? delay : 0;
            particle.position.set( 0, 0, 0 );
            particle.scale.x = particle.scale.y = 5;

            new TWEEN.Tween( particle.position )
                .delay( delay )
                .to( { x: Math.random() * 20, y: 1, z: Math.random() * 2 }, 500 )
                .start();
            new TWEEN.Tween( particle.scale )
                .delay( delay )
                .to( { x: 0.01, y: 0.01 }, 500)
                .start();
        }

        for ( var i = 0; i < 1000; i++ ) {
            var particle = new THREE.Sprite( particleMaterial );
            initParticle( particle, i * (distance / 50) );
            frostBolt.add( particle );
        }
    }

    /**
     * The render loop.
     */
    function onAnimationFrame() {
        requestAnimationFrame(onAnimationFrame);

        cameraController.update();

        // update moving light position
        lights.movingLight.position.x = camera.position.x;
        lights.movingLight.position.z = camera.position.z;

        TWEEN.update();
        renderer.render(scene, camera);
    }

    /**
     * Converts the board position to 3D world position.
     * @param {Array} pos The board position.
     * @returns {THREE.Vector3}
     */
    function boardToWorld(pos) {
        var x = (1 + pos[1]) * squareSize - squareSize / 2;
        var z = (1 + pos[0]) * squareSize - squareSize / 2;

        return new THREE.Vector3(x, 0, z);
    }

    //MOUSE INTERACT
    document.addEventListener('mousedown', onDocumentMouseDown, false);
    document.addEventListener('mousemove', onDocumentMouseMove, false);


    var raycaster = new THREE.Raycaster();
    var mouse = new THREE.Vector3();
    mouse.z = 0.5;

    function onDocumentMouseMove(event) {
        event.preventDefault();

        mouse.x = ( event.clientX / window.innerWidth ) * 2 - 1;
        mouse.y = -( event.clientY / window.innerHeight ) * 2 + 1;

        if (LUDO.Game.started && self.inGame && self.yourTurn) {

            raycaster.setFromCamera(mouse, camera);
            var intersects = raycaster.intersectObjects(scene.children, true);

            if (intersects.length > 0) {
                pickedPawn = intersects[0].object.parent;
                if ( pickedPawn != INTERSECTED ) {
                    INTERSECTED = pickedPawn;
                    if(savedMat != null) {
                        pickedPawn_.children[0].material = savedMat;
                        savedMat = null;
                    }

                    //JEZELI NAMIERZONO PIONEK
                    if (pickedPawn.name === "Pawn") {
                        //JEZELI TO TWOJ PIONEK
                        if (pickedPawn.color === self.playerColor) {
                            if (rolledNumber !== 0) {
                                socket.emit('getPawnPath', pickedPawn.boardId, function (destination) {
                                    if (destination !== false)
                                        moveParticlesRing(true, destination[0], destination[1]);
                                });
                            }

                        } else {
                            if ($('#menu').find('img').hasClass('active')) {
                                socket.emit('havePowerups', {}, function (havePower) {
                                    if (havePower) {
                                        pickedPawn_ = pickedPawn;
                                        savedMat = pickedPawn_.children[0].material;
                                        var tempMat = pickedPawn_.children[0].material.clone();
                                        tempMat.transparent = true;
                                        pickedPawn_.children[0].material = tempMat;
                                    }
                                });
                            }
                        }

                    } else {
                        moveParticlesRing(false);
                    }
                }
            }
        }
    }

    function onDocumentMouseDown(event) {
        event.preventDefault();

        socket.emit('playerInGame', {}, function (inGame) {
            self.inGame = inGame;
        });

        if (self.inGame) {
            raycaster.setFromCamera(mouse, camera);

            var intersects = raycaster.intersectObjects(scene.children, true);
            if (intersects.length > 0) {
                pickedPawn = intersects[0].object.parent;


                //JEZELI KNIKNIETO PIONEK
                if (pickedPawn.name === "Pawn") {

                    //WYBIERANIE PIONKOW -JEZELI JESZCZE GRACZ NIE MA PRZYPISANEGO KOLORU
                    if (self.playerColor === null) {
                        var pickedColor = pickedPawn.color;

                        socket.emit('colorIsAvailable', {color: pickedColor}, function (available) {
                            if (available) {
                                self.playerColor = pickedColor;
                                setPlayerData(pickedColor);
                            } else {
                                showAnnotation("Ten kolor jest zajęty !\nWybierz inny", 2000);
                            }
                        });
                    }
                    if (self.yourTurn) {

                        if (pickedPawn.color === self.playerColor) {
                            //PROBA RUCHU PRZY TURZE BEZ WYLOSOWANIA CYFRY
                            if (rolledNumber === 0) {
                                showAnnotation("Najpierw wylosuj cyfrę !");
                            } else {
                                socket.emit('movePawn', pickedPawn.boardId, function (msg) {
                                    if (msg)
                                        showAnnotation(msg);
                                });
                            }
                        } else if ($('#menu').find('img').hasClass('active')) {
                            socket.emit('havePowerups', {}, function (havePower) {
                                if (havePower) {
                                    $('#menu').find('img').removeClass('active');
                                    pickedPawn_.children[0].material = savedMat;
                                    savedMat = null;
                                    frostBoltShoot(5,5,pickedPawn.boardPosX,pickedPawn.boardPosY);
                                    socket.emit('frostBoltShoot', pickedPawn_.boardId);
                                }
                            });
                        }
                    } else if (!LUDO.Game.started) {
                        showAnnotation("Gra się jeszcze nie rozpoczęła");
                    } else {
                        showAnnotation("To nie jest Twoja tura\nPoczekaj na swoją kolej");
                    }

                }
            }
        }
    }

    socket.on('movePawn', function (data) {
        var pickedPawn = findPawn(data.pawnId);

        if (data.result === true) {
            movePawn(pickedPawn, data.desireX, data.desireY);
            pickedPawn.boardPosX = data.desireX;
            pickedPawn.boardPosY = data.desireY;
        } else if (data.result === false) {
            showAnnotation(data.msg);
        }
    });

    socket.on('powerupGenerator', function (power) {
        if(power.name == "Frost Bolt") {
            generatePowerup(power.position.x, power.position.y);
        }
    });

    socket.on('powerupRemove', function (power) {
        removePowerup(power.position.x, power.position.y)
    });

    function removePowerup(x,y) {
        scene.children.forEach(function (obj) {
            if (obj.type === "power")
                if (obj.boardX == x && obj.boardY == y)
                    scene.remove(obj);
        });
    }

    function generatePowerup(x, y) {
        var frostBolt = new THREE.Object3D();
        frostBolt.type = "power";
        frostBolt.boardX = x;
        frostBolt.boardY = y;
        frostBolt.position.set(boardToWorld([x, y]).x, 1, boardToWorld([x, y]).z);
        scene.add(frostBolt);

        function powerAnimation() {
            var tween = new TWEEN.Tween(frostBolt.rotation)
                .to({y: Math.PI}, 5000)
                .onComplete(function () {
                    frostBolt.rotation.y = 0;
                    powerAnimation();
                })
                .start();
        }
        powerAnimation();

        var distance = 1.5;
        var geometry = new THREE.Geometry();

        for (var i = 0; i < 1000; i++) {

            var vertex = new THREE.Sprite( particleMaterial );

            var theta = THREE.Math.randFloatSpread(360);
            var phi = THREE.Math.randFloatSpread(360);

            vertex.x = distance * Math.sin(theta) * Math.cos(phi);
            vertex.y = distance * Math.sin(theta) * Math.sin(phi);
            vertex.z = distance * Math.cos(theta);

            geometry.vertices.push(vertex);
        }
        var particles = new THREE.Points(geometry, new THREE.PointsMaterial({

            color: 0x00ddff,
            size: 4,
            blending: THREE.AdditiveBlending,
            transparent: true,
            sizeAttenuation: false
        }));

        particles.boundingSphere = 10;

        frostBolt.add(particles);
    }

    this.generatePowerupRefresh = function(powerups) {
        powerups.forEach(function(power){
            generatePowerup(power.position.x, power.position.y);
        });
    };

    function moveParticlesRing(visible, x, y) {
        particlesRing.visible = visible;

        if(typeof x !== 'undefined') {
            particlesRing.position.set(boardToWorld([x, y]).x, 1, boardToWorld([x, y]).z);
        }
    }

    function findPawn(id) {
        scene.children.forEach(function (pawn) {
            if (pawn.name === "Pawn")
                if (pawn.boardId === id)
                    foundPawn = pawn;
        });

        return foundPawn;
    }

    function setPlayerData(color) {
        socket.emit('setPlayerData', color, function (msg) {
            showAnnotation(msg, 2000);
        });
    }

    function movePawn(pawn, x, y) {
        pawn.position.z = (x + 1) * squareSize - squareSize / 2;
        pawn.position.x = (y + 1) * squareSize - squareSize / 2;
        moveParticlesRing(false);
    }

    this.setRolledNumber = function () {
        socket.emit('getRolledNumber', {}, function (number) {
            rolledNumber = number;
        });
        return rolledNumber;
    };

};
