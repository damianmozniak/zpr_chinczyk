var LUDO = {
    BLUE: 1,
    RED: 2,
    YELLOW: 3,
    GREEN: 4
};

var annotations = $('#annotations');

function showAnnotation(text, time) {
    var time = time ? time : 1000;
    $("#menu").notify(text,{
        position: 'left',
        className: "info",
        showDuration: time,
        hideDuration: 500,
        arrowShow: false
    });
}

/** @type LUDO.BoardController */
var boardController = null;

LUDO.Game = function (options) {
    'use strict';

    LUDO.Game.options = options || {};

    this.started = false;
    this.isPlayerTurn = false;

    /**********************************************************************************************/
    /* Private methods ****************************************************************************/
    /**
     * Initializer.
     */
    (function init(fcallback) {
        getInfo();
        fcallback();
    })(function(){
        boardController = new LUDO.BoardController({
            containerEl: options.containerEl,
            assetsUrl: options.assetsUrl,
        });

        boardController.drawBoard(boardController.setPawnsOnBoard);
    });
};

$(function() {
    $(document).keydown(function(evt) {
        if (LUDO.Game.started && evt.keyCode == 32) {
            rollTheDice();
        }
        if(evt.keyCode == 83) {
            console.log("super6 Switch");
            socket.emit('super6');
        }
    });
});

var FizzyText = function () {

    this.Losuj = function () {
        rollTheDice();
    };

    this.startGame = function () {
        socket.emit('startGame');
    };

    this.join = function () {
        socket.emit('joinPlayer');
        gui.remove(joinButton);
        getInfo();
        showAnnotation("Dołącześ do gry\nWybierz swoje pionki");
    };

    this.player = function () {};
};

var text;
var joinButton;
var startGameButton;

window.onload = function () {
    window.gui = new dat.GUI();
    text = new FizzyText();
};

const socket = io.connect(window.location.href);

socket.on('connect', function () {
    console.log('Successfully connected!');
});

var tempLabels = [];

socket.on('playerJoined', function (data) {
    showAnnotation("Dołączył gracz " + data.players, 1000);

    tempLabels.push(gui.add(text, "player").name("Gracz " + data.players));
});

socket.on('winnerAnnotation', function (color) {
    var textMessage = "Gratulacje ! Wygrał gracz ";

    switch (color) {
        case 1:
            textMessage += "Niebieski";
            break;
        case 2:
            textMessage += "Czerwony";
            break;
        case 3:
            textMessage += "Żółty";
            break;
        case 4:
            textMessage += "Zielony";
            break;
        default:
            break;
    }

    showAnnotation(textMessage, 60000);
});

socket.on('gameStarted', function (started) {
    if (started) {
        LUDO.Game.started = true;
        showAnnotation("Gra rozpoczęta !\nZaczyna Gracz 1", 1000);
        gui.remove(startGameButton);
        getInfo();
    } else {
        showAnnotation("Nie wszyscy wybrali pionki\nProszę czekać");
    }
});

function rollTheDice(){
    socket.emit('rollTheDice', {}, function(rolledNumber){
        if(!rolledNumber.winner)
            boardController.rollTheDice(rolledNumber);
        else
            showAnnotation( "Gra zakończona. Wygrał gracz " + rolledNumber.winner,50000)
    });
}


function getInfo() {
    socket.emit('getGameInfo', {}, function (gameStarted, playerInGame, numOfPlayers, playerObject, powerups, isPlayerTurn) {
        tempLabels.forEach(function (label) {
            gui.remove(label);
        });

        tempLabels = [];
        if (!gameStarted) {
            if (playerInGame)
                startGameButton = gui.add(text, 'startGame').name('Zacznij grę');
            if (!playerInGame)
                joinButton = gui.add(text, "join").name("Dołącz");

        } else {
            tempLabels.push(gui.add(text, "Losuj"));
            LUDO.Game.started = true;
            setIsPlayerTurn();
            if(isPlayerTurn)
                boardController.setRolledNumber();
        }

        if (playerInGame) {
            boardController.inGame = true;
            boardController.playerColor = playerObject.color;
        }

        for (var i = 1; i <= numOfPlayers; i++)
            tempLabels.push(gui.add(text, "player").name("Gracz " + i));

        if(powerups != null)
            boardController.generatePowerupRefresh(powerups);
    });
}
(function createPowerIcons() {
    $('#menu').find('img').click(function() {
        if(LUDO.Game.started) {
            socket.emit('havePowerups', {}, function (havePower) {
                if(havePower) {
                    $('#menu').find('img').toggleClass('active');
                } else {
                    showAnnotation("Niestety nie posiadasz Frost Bolt'a\nPoczekaj aż się pojawi i spróbuj go zebrać!");
                }
            });
        }
    });
})();

function setIsPlayerTurn() {
    socket.emit('isPlayerTurn', {}, function (isPlayerTurn) {
        boardController.yourTurn = isPlayerTurn;
    });
}



