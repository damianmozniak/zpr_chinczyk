var Player = function(ip) {

    this.ip = ip;
    this.color = null;
    this.StartPoint = {
        x: 0,
        y: 0
    };
    this.FinishPoints = [
        {x: 0, y: 0},
        {x: 0, y: 0},
        {x: 0, y: 0},
        {x: 0, y: 0}
    ];

    this.pawns = [];

    this.powers = [];
};

module.exports = Player;