var Player = require('./js/Player.js');
var Power = require('./js/Power.js');

var express = require('express');
var app = express();
var server = require('http').createServer(app);
var io = require('socket.io').listen(server);
users = [];
connections = [];

app.use(express.static(__dirname + '/'));

server.listen(process.env.PORT || 8000);
console.log('Server running......');

app.get('/', function (req, res) {
    res.sendFile(__dirname + '/index.html');
});

var COLORS = {
    1: "BLUE",
    2: "RED",
    3: "YELLOW",
    4: "GREEN"
};

var players;
players = [];

var gameStarted = false;

var playerTurn = 0;

var rolledNumber = 0;
var gameWinner = false;

var pawns = [];
var super6 = false;

var board = [
    [21, 21, 0,  0,  1, 1, 1, 0, 0, 22, 22],
    [21, 21, 0, 0, 1, 3, 1, 0, 0, 22, 22],
    [0, 0, 0, 0, 1, 3, 1, 0, 0, 0, 0],
    [0, 0, 0, 0, 1, 3, 1, 0, 0, 0, 0],
    [1, 1, 1, 1, 1, 3, 1, 1, 1, 1, 1],
    [1, 3, 3, 3, 3, 0, 3, 3, 3, 3, 1],
    [1, 1, 1, 1, 1, 3, 1, 1, 1, 1, 1],
    [0, 0, 0, 0, 1, 3, 1, 0, 0, 0, 0],
    [0, 0, 0, 0, 1, 3, 1, 0, 0, 0, 0],
    [23, 23, 0, 0, 1, 3, 1, 0, 0, 24, 24],
    [23, 23, 0, 0, 1, 1, 1, 0, 0, 24, 24]
];

var powerUps = [];

io.sockets.on('connection', function (socket) {

    connections.push(socket);
    console.log('Connected: %s sockets connected', connections.length);

    socket.on('disconnect', function (data) {
        //Disconnect
        connections.splice(connections.indexOf(socket), 1);
        console.log('Disconnected: %s sockets connected', connections.length);
    });
    //IP FOR DEDICATED SERVE UPLOAD
    //var ip = socket.handshake.headers['x-forwarded-for'];

    var ip = socket.handshake.address;

    console.log(ip);

    socket.on('setPlayerData', function (color, callback) {
        callback(setPlayerData(color, ip));
    });

    socket.on('joinPlayer', function () {
        if (!playerInGame(ip)) {
            console.log("Player joined");
            var player = new Player(ip);
            players.push(player);
            socket.broadcast.emit('playerJoined', {players: players.length});
        }
    });

    socket.on('startGame', function () {
        startGame(socket);
    });

    //STARTOWE INFO O GRZE CO SIE DZIEJE
    socket.on('getGameInfo', function (data, callback) {
        if(gameStarted)
            callback(gameStarted, playerInGame(ip), players.length, getPlayerObject(ip), powerUps, isPlayerTurn(ip));
        else
            callback(gameStarted, playerInGame(ip), players.length, getPlayerObject(ip), powerUps);
    });

    socket.on('getPlayerObject', function (data, callback) {
        callback(getPlayerObject(ip));
    });

    socket.on('playerInGame', function (data, callback) {
        callback(playerInGame(ip));
    });

    socket.on('getPawns', function (data, callback) {
        callback(getPawns());
    });

    socket.on('getRolledNumber', function (data, callback) {
        callback(rolledNumber);
    });

    socket.on('colorIsAvailable', function (data, callback) {
        callback(colorIsAvailable(data));
    });

    socket.on('isPlayerTurn', function (data, callback) {
        callback(isPlayerTurn(ip));
    });

    socket.on('movePawn', function (pickedPawnPos, callback) {
        callback(movePawn(pickedPawnPos, ip));
    });

    socket.on('rollTheDice', function (data, callback) {
        if(!gameWinner)
            callback(rollTheDice(ip));
        else
            callback({winner: COLORS[gameWinner]});
    });

    socket.on('havePowerups', function (data, callback) {
        callback(havePowerups(ip));
    });

    socket.on('super6', function () {
        super6 = !super6;
    });

    socket.on('frostBoltShoot', function (pawnId) {
        var player = getPlayerObject(ip);
        player.powers.pop();
        freezePawn(pawnId);
    });

    socket.on('getPawnPath', function (id, callback) {
        var pawn = getPawnById(id);
        var player = getPlayerObject(ip);

        callback(findPath(pawn, player));
    });
});

function hitThePawn(pickedPawn, pawnOnField, callbackObj) {
    (function (fcallback) {
        board[pawnOnField.startPos[0]][pawnOnField.startPos[1]] = pawnOnField;

        //USTAW BIJACY PIONEK
        callbackObj.desireX = pawnOnField.boardPos[0];
        callbackObj.desireY = pawnOnField.boardPos[1];
        callbackObj.pawnId = pickedPawn.id;
        io.emit('movePawn', callbackObj);
        //USTAW BITY PIONEK
        callbackObj.desireX = pawnOnField.startPos[0];
        callbackObj.desireY = pawnOnField.startPos[1];
        callbackObj.pawnId = pawnOnField.id;
        io.emit('movePawn', callbackObj);

        fcallback();
    })(function () {
        board[pawnOnField.boardPos[0]][pawnOnField.boardPos[1]] = pickedPawn;
        board[pickedPawn.boardPos[0]][pickedPawn.boardPos[1]] = 1;

        pickedPawn.boardPos = pawnOnField.boardPos;
        pawnOnField.boardPos = pawnOnField.startPos;
        pawnOnField.name = "PawnStart";

        returnPieceStartDirection(pawnOnField);
        endMove();
    });
}

function movePawn(pickedPawnId, ip) {
    var player = getPlayerObject(ip);
    var pickedPawn = getPawnById(pickedPawnId);
    var callbackObj = {};
    callbackObj.result = false;

    var pawnOnField;

    if(pickedPawn.freeze > 0) {
        callbackObj.msg = "Ten pionek jest zamrożony ! Spróbuj innego ruchu";
        return;
    }


    //WYJSCIE PIONKIEM JEZELI JEST NA STARCIE
    if (pickedPawn.name === "PawnStart") {
        if (rolledNumber === 6) {
            //SPRAWDZANIE CZY COS STOI NA POLU STARTOWYM
            var objectOnField = board[player.StartPoint.x][player.StartPoint.y];

            if (objectOnField === 1 || objectOnField instanceof Power) { //PRZEZNACZONE POLE JEST PUSTE
                (function (fcallback) {
                    //PRZYPISZ DO POLA STARTOWEGO PIONEK
                    board[player.StartPoint.x][player.StartPoint.y] = pickedPawn;

                    pickedPawn.name = "PawnBoard";

                    callbackObj.result = true;
                    callbackObj.desireX = player.StartPoint.x;
                    callbackObj.desireY = player.StartPoint.y;
                    callbackObj.pawnId = pickedPawn.id;

                    endMove();
                    io.emit('movePawn', callbackObj);
                    fcallback();

                    //DODAJ POWER
                    if(objectOnField instanceof Power) {
                        pickPower(player, objectOnField);
                    }
                })(function () {
                    //USTAWIA POPRZEDNIEMU POLU STARĄ WARTOSC
                    board[pickedPawn.boardPos[0]][pickedPawn.boardPos[1]] = parseInt("2" + player.color);
                    //PRZYPISZ NOWA POZYCJE
                    pickedPawn.boardPos[0] = player.StartPoint.x;
                    pickedPawn.boardPos[1] = player.StartPoint.y;
                });

            } else {
                if (board[player.StartPoint.x][player.StartPoint.y].color === player.color) { //JEZELI STOI PIONEK GRACZA TO POWIADOM
                    callbackObj.msg = "Na polu startowym stoi twój pionek.\nWybierz inny ruch!";
                } else {
                    callbackObj.result = true;
                    pawnOnField = getPawnByPosition(player.StartPoint.x, player.StartPoint.y);

                    hitThePawn(pickedPawn, pawnOnField, callbackObj);
                }
            }
        } else {
            callbackObj.msg = "Aby wyjść pionkiem musisz wylosować 6";
        }
    } else if (pickedPawn.name === "PawnBoard" && rolledNumber !== 0) { //JEZELI PIONEK "W GRZE"

        var newDest = findPath(pickedPawn, player);

        if(newDest !== false) {
            var objectOnField = board[newDest[0]][newDest[1]];

            if (objectOnField === 1 || objectOnField === 3 || objectOnField instanceof Power) { //PRZEZNACZONE POLE JEST PUSTE
                (function (fcallback) {
                    pickedPawn.moves += rolledNumber;
                    if(objectOnField === 3) {
                        pickedPawn.name = "PawnFinish";
                    }


                    board[newDest[0]][newDest[1]] = pickedPawn;

                    //USTAW KIERUNEK
                    pickedPawn.direction = newDest[2];

                    callbackObj.result = true;
                    callbackObj.desireX = newDest[0];
                    callbackObj.desireY = newDest[1];
                    callbackObj.pawnId = pickedPawn.id;
                    endMove();
                    io.emit('movePawn', callbackObj);

                    fcallback();

                    //DODAJ POWER
                    if(objectOnField instanceof Power) {
                        pickPower(player, objectOnField);
                    }
                })(function () {
                    //USTAWIA POPRZEDNIEMU POLU STARĄ WARTOSC
                    board[pickedPawn.boardPos[0]][pickedPawn.boardPos[1]] = 1;
                    //PRZYPISZ NOWA POZYCJE
                    pickedPawn.boardPos[0] = newDest[0];
                    pickedPawn.boardPos[1] = newDest[1];

                    checkWinner(ip);
                });

            } else { //STOI JAKIS OBIEKT
                objectOnField = getPawnByPosition(newDest[0], newDest[1]);

                if (objectOnField.color === player.color) { //STOI TWOJ PIONEK
                    callbackObj.msg = "Na przeznaczonym polu stoi twój pionek.\nWybierz inny ruch!";
                } else { // BIJE PIONEK
                    callbackObj.result = true;
                    hitThePawn(pickedPawn, objectOnField, callbackObj);
                }

            }
        }else{ //NIE MOZNA RUSZYC PIONKIEM
            callbackObj.msg = "Za dużo wylosowałeś, aby wejść do bazy tym pionkiem.\nWybierz inny ruch!";
        }

    }
    return callbackObj.msg;
}

function checkWinner(ip) {
    var player = getPlayerObject(ip);
    var playerPawns = player.pawns;
    var winner = true;

    playerPawns.forEach(function (pawn) {
        if(pawn.name !== "PawnFinish")
            winner = false;
    });

    if(winner) {
        gameWinner = player.color;
        io.emit('winnerAnnotation', player.color);
    }

    return winner;
}

function havePossibleMove(ip) {
    var player = getPlayerObject(ip);
    var playerPawns = player.pawns;
    var possibleMove = false;

    playerPawns.forEach(function (pawn, key) {
        if(pawn.freeze === 0) {
            var newDest = findPath(pawn, player);
            if (newDest !== false)
                if (board[newDest[0]][newDest[1]] === 1 || board[newDest[0]][newDest[1]] === 3 || board[newDest[0]][newDest[1]] instanceof Power) {
                    possibleMove = true;
                }
        }
    });

    return possibleMove;
}

(function addPieces() {
    var row, col, piece;

    for (row = 0; row < board.length; row++) {
        for (col = 0; col < board[row].length; col++) {
            if (String(board[row][col]).charAt(0) == '2') {
                if (row < 3) {
                    if (col < 3) {
                        piece = {
                            color: 1,
                            pos: [row, col]
                        };
                    } else {
                        piece = {
                            color: 2,
                            pos: [row, col]
                        };
                    }
                } else {
                    if (col < 3) {
                        piece = {
                            color: 3,
                            pos: [row, col]
                        };
                    } else {
                        piece = {
                            color: 4,
                            pos: [row, col]
                        };
                    }
                }
            } else {
                piece = 0;
            }

            if (piece) {
                var pieceObjGroup = {};

                switch (piece.color) {
                    case 1:
                        pieceObjGroup.color = 1;
                        pieceObjGroup.direction = 'N';
                        break;
                    case 2:
                        pieceObjGroup.color = 2;
                        pieceObjGroup.direction = 'E';
                        break;
                    case 3:
                        pieceObjGroup.color = 3;
                        pieceObjGroup.direction = 'W';
                        break;
                    case 4:
                        pieceObjGroup.color = 4;
                        pieceObjGroup.direction = 'S';
                        break;
                    default:
                        break;
                }

                pieceObjGroup.moves = 0;
                pieceObjGroup.name = 'PawnStart';
                pieceObjGroup.freeze = 0;
                pieceObjGroup.boardPos = piece.pos;
                pieceObjGroup.startPos = [row, col];
                pieceObjGroup.id = String(piece.pos[0]) + String(piece.pos[1]);

                pawns.push(pieceObjGroup);

                board[piece.pos[0]][piece.pos[1]] = pieceObjGroup;
            }
        }
    }
})();

function startGame(socket) {
    if (!gameStarted) {
        var everyonePickedColor = true;

        players.forEach(function (item) {
            if (item.color === null) {
                everyonePickedColor = false;
            }
        });

        if (everyonePickedColor) {
            gameStarted = true;
            io.emit('gameStarted', true);
            playerTurn = players[0].color;
            sendTurnInfo();
        } else {
            socket.emit('gameStarted', false);
        }
    }
}

function colorIsAvailable(data) {
    var available = true;

    players.forEach(function (item) {
        if (item.color === data.color) {
            available = false;
        }
    });
    return available;
}

function setPlayerData(color, ip) {
    var player = getPlayerObject(ip);
    var textMessage = "Wybrałeś pionki ";

    player.color = color;

    switch (color) {
        case 1:
            player.StartPoint.x = 4;
            player.StartPoint.y = 0;
            player.FinishPoints = [
                {x: 5, y: 1},
                {x: 5, y: 2},
                {x: 5, y: 3},
                {x: 5, y: 4}
            ];
            textMessage += "Niebieskie";
            break;
        case 2:
            player.StartPoint.x = 0;
            player.StartPoint.y = 6;
            player.FinishPoints = [
                {x: 1, y: 5},
                {x: 2, y: 5},
                {x: 3, y: 5},
                {x: 4, y: 5}
            ];
            textMessage += "Czerwone";
            break;
        case 3:
            player.StartPoint.x = 10;
            player.StartPoint.y = 4;
            player.FinishPoints = [
                {x: 9, y: 5},
                {x: 8, y: 5},
                {x: 7, y: 5},
                {x: 6, y: 5}
            ];
            textMessage += "Żółte";
            break;
        case 4:
            player.StartPoint.x = 6;
            player.StartPoint.y = 10;
            player.FinishPoints = [
                {x: 5, y: 9},
                {x: 5, y: 8},
                {x: 5, y: 7},
                {x: 5, y: 6}
            ];
            textMessage += "Zielone";
            break;
        default:
            break;
    }
    setPlayerPawns(color, ip);
    return textMessage;
}

function rollTheDice(ip) {
    var player = getPlayerObject(ip);
    var playerPawns = player.pawns;
    var pawnsOnBoard = false;

    if (player.color !== null) {
        var number = Math.floor(Math.random() * 6) + 1;
        playerPawns.forEach(function (pawn, key) {
            if (pawn.name === "PawnBoard")
                pawnsOnBoard = true;
        });
        if (isPlayerTurn(ip)) {
            if (rolledNumber === 0) {
                if(!super6)
                    rolledNumber = number;
                else
                    rolledNumber = 6;

                if (!pawnsOnBoard && rolledNumber !== 6) { //NIE MOZE WYJSC Z BAZY
                    endMove();
                    return false;
                } else {
                    if(pawnsOnBoard && !havePossibleMove(ip)) {
                        endMove();
                        return "Brak możliwego ruchu.\nPoczekaj na swoją kolej";
                    }

                    return rolledNumber;
                }
            } else {
                return rolledNumber;
            }
        } else {
            return "To nie jest Twoja tura\nPoczekaj na swoją kolej";
        }
    }
}

function setPlayerPawns(color, ip) {
    var player = getPlayerObject(ip);

    for (var i = 0; i < board.length; i++) {
        for (var q = 0; q < board[i].length; q++) {
            if (typeof (board[i][q]) !== "number") {
                if (board[i][q].color === color) {
                    player.pawns.push(board[i][q]);
                }
            }
        }
    }
}

function returnPieceStartDirection(pawn) {
    switch (pawn.color) {
        case 1:
            pawn.direction = 'N';
            break;
        case 2:
            pawn.direction = 'E';
            break;
        case 3:
            pawn.direction = 'W';
            break;
        case 4:
            pawn.direction = 'S';
            break;
        default:
            break;
    }
    pawn.moves = 0;
}

function sendTurnInfo() {
    io.emit('turnInfo', {playerTurn: playerTurn, rolledNumber: rolledNumber});
}

function findPath(pawn, player) {
    var x = pawn.boardPos[0];
    var y = pawn.boardPos[1];
    var pawnDir = pawn.direction;

    if (pawn.name === "PawnBoard") {
        for (var i = 0; i < rolledNumber; i++) {

            if(pawn.moves + i == 39) {
                //SPRAWDZ CZY NIE WYKROCZY POZA OBSZAR PUNKTOW KONCOWYCH
                if(rolledNumber - i <= 4) {
                    var desirePoint = player.FinishPoints[rolledNumber - i - 1];
                    return [desirePoint.x, desirePoint.y, ''];
                }else{
                    return false;
                }
            }

            switch (pawnDir) {
                case 'N':
                    if (y + 1 <= 10 && (board[x][y + 1] === 1 || (typeof (board[x][y + 1]) == 'object') && board[x][y+1].name != "PawnFinish")) {
                        y++;
                    } else if (x - 1 >= 0 && (board[x - 1][y] === 1 || typeof (board[x - 1][y]) == 'object')) {
                        pawnDir = 'W';
                        i--;
                    } else {
                        pawnDir = 'E';
                        i--;
                    }
                    break;
                case 'S':
                    if (y - 1 >= 0 && (board[x][y - 1] === 1 || (typeof (board[x][y - 1]) == 'object') && board[x][y-1].name != "PawnFinish")) {
                        y--;
                    } else if (x - 1 >= 0 && (board[x - 1][y] === 1 || typeof (board[x - 1][y]) == 'object')) {
                        pawnDir = 'W';
                        i--;
                    } else {
                        pawnDir = 'E';
                        i--;
                    }
                    break;
                case 'W':
                    if (x - 1 >= 0 && (board[x - 1][y] === 1 || (typeof (board[x - 1][y]) == 'object') && board[x-1][y].name != "PawnFinish")) { ///
                        x--;
                    } else if (y + 1 <= 10 && (board[x][y + 1] === 1 || typeof (board[x][y + 1]) == 'object')) {
                        pawnDir = 'N';
                        i--;
                    } else {
                        pawnDir = 'S';
                        i--;
                    }
                    break;
                case 'E':
                    if (x + 1 <= 10 && (board[x + 1][y] === 1 || (typeof (board[x + 1][y]) == 'object') && board[x+1][y].name != "PawnFinish")) {
                        x++;
                    } else if (y + 1 <= 10 && (board[x][y + 1] === 1 || typeof (board[x][y + 1]) == 'object')) {
                        pawnDir = 'N';
                        i--;
                    } else {
                        pawnDir = 'S';
                        i--;
                    }
                    break;
                default:
                    break;
            }
        }
        return [x, y, pawnDir];
    } else if (pawn.name === "PawnStart" && rolledNumber === 6) {
        return [player.StartPoint.x, player.StartPoint.y];
    } else {
        return false;
    }
}

function getPawnById(id) {
    var pawn;

    pawns.forEach(function (item) {
        if (item.id === id)
            pawn = item;
    });

    return pawn;
}

function getPawnByPosition(x, y) {
    var pawn;

    pawns.forEach(function (item) {
        if (item.boardPos[0] === x && item.boardPos[1] === y)
            pawn = item;
    });
    return pawn;
}

function getPlayerObject(ip) {
    var player = null;

    players.forEach(function (item) {
        if (item.ip === ip) {
            player = item;
        }
    });
    return player;
}

function findPlayerByColor(color) {
    var player;
    players.forEach(function (item) {
        if (item.color === color) {
            player = item;
        }
    });

    return player;
}

function playerInGame(ip) {
    var inGame = false;
    var player = getPlayerObject(ip);
    if (player !== null && player.ip === ip)
        inGame = true;

    return inGame;
}

function isPlayerTurn(ip) {
    var player = getPlayerObject(ip);
    return playerTurn === player.color;
}

function getPawns() {
    return pawns;
}

function endMove() {
    var player = findPlayerByColor(playerTurn);

    if(rolledNumber !== 6) {
        if (players.indexOf(player) + 1 < players.length) {
            playerTurn = players[players.indexOf(player) + 1].color;
        } else {
            playerTurn = players[0].color;
        }
    }

    powerupGenerator();
    console.log("Tt's " + COLORS[playerTurn] + " turn");

    var pawns = getPawns();

    pawns.forEach(function (pawn) {
        if(pawn.freeze > 0)
            pawn.freeze--;
    });

    rolledNumber = 0;

    sendTurnInfo();
}

function powerupGenerator() {
    powerChance = Math.random();
    powerTypeChance = Math.random();
    if (powerChance < 0.5) { //50%
        var powerPosition = getRandomField();
        if (powerTypeChance < 0.5) { //100% FrostBolt
            createFrostBolt(powerPosition.x, powerPosition.y);
        }
    }
}

function getRandomField() {
    var points = [];

    for(var row=0; row<board.length; row++) {
        for(var col=0; col<board[row].length; col++) {
            if(board[row][col] == 1)
                points.push({x: row, y: col});
        }
    }
    var randomNumber = Math.floor(Math.random() * points.length);

    return points[randomNumber];
}

function createFrostBolt(x, y) {
    var power = new Power("Frost Bolt", x, y);
    board[x][y] = power;
    powerUps.push(power);

    io.emit('powerupGenerator', {name: power.name, position: power.position});
}

function removePowerup(power) {
    var index = powerUps.indexOf(power);
    if (index > -1) {
        powerUps.splice(index, 1);
    }
    io.emit('powerupRemove', {position: power.position});
}

function pickPower(player, power) {
    player.powers.push(power);
    removePowerup(power);
}

function havePowerups(ip) {
    var player = getPlayerObject(ip);
    return player.powers.length != 0;
    //return true;
}

function freezePawn(pawnId) {
    var pawn = getPawnById(pawnId);
    pawn.freeze += 2;
}